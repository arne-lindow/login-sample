# login-sample

```
git clone https://arne-lindow@bitbucket.org/arne-lindow/login-sample.git
```

```
cd login-sample
```

## Test First!

### Test Framework
Als TestFramework wird [bunit](https://github.com/rafritts/bunit bunit) genutzt.

## Implementation
Shell Skript

## Run Unit Tests
```
./check.ut -v
```

## Usage
```
./main.sh <login> <password>
```

## Vorbedingung
Es muss eine logins Datei existieren, die kann jeder Admin mit folgendem Kommando erstellen:
```
echo "loginname,$(echo passwort | md5sum | cut -d ' ' -f 1)" >> logins
```

## Setting up keycloak in 3 simple steps:
1. docker run -p 8080:8080 jboss/keycloak
2. docker ps -a
3. docker exec 65b9b5787e97 keycloak/bin/add-user-keycloak.sh -u admin -p keycloak
4. docker restart 65b9b5787e97
