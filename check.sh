#!/bin/bash

main() {
    if [[ $# != 2 ]]
    then
	echo "Failed!"
	exit 1
    fi
    login=$1
    passwort=$(echo $2 | md5sum | cut -d ' ' -f 1)
    zeile=$(grep "$login," logins)
    exptected=$(echo $zeile | cut -d ',' -f 2)
    if [[ $passwort == $exptected ]]
    then
	echo SUCCESS: You are logged in
	exit 0
    else
	echo "Failed!"
	exit 1
    fi
}
